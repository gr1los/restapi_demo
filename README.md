# restApi_demo
This is a REST api demo using Node.js, Express.js and MongoDB (mongoose)

HTTP Verb | Data | Description
---------:| :----- |:-----:
GET|empty|Receive a test response|
GET|:id|Receive information for the specified id
POST|:name , :surname , :phone|Creates a new user
PUT|:id , :name , :surname , :phone|Updates an existing user
DELETE|:id|Deletes the user of the specified id

### How to build?
* npm install
* npm start

### How to use?
* npm start