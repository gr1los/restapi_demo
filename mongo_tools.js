/**
 * Created by gabriel on 6/22/16.
 */

var mongoose = require('mongoose');

var options = {
    db: { native_parser: true },
    server: { poolSize: 5, serverOptions: { keepAlive: 120} },
};

// Init mongoose Schema
var customerSchema = mongoose.Schema({
    name:       String,
    surname:    String,
    phone:      String
});
var Customer = mongoose.model("Customer", customerSchema);

// createOutput function, constructs the response message
function createOutput(opSucceed, msg){
    return {
        status: opSucceed ? "success" : "failed",
        message: msg
    };
};

// Assistance functions for mongodb
var mongoTools = function (){
    // Init connection to mongodb
    mongoose.connect('mongodb://localhost/test', options);

    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {console.log("Connected on MongoDB(localhost) !")});

    var self = this;

    self.addUser = function (params, res){      // Adds new user
        var newCustomer =  new Customer({
            name:       params.name,
            surname:    params.surname,
            phone:      params.phone
        });

        newCustomer.save(function (err, customer) {
            if (err) return res.status(500).json(createOutput(false, err));
            if (customer == null) return res.status(404).json(createOutput(false, {}));
            res.status(201).json(createOutput(true, {id : customer._id}));
        });
    };

    self.findUser = function(objectID, res){    // Finds User by id
        // callback is missing, we dont want to execute immediately
        var query = Customer.findById({_id: objectID});

        query.exec(function (err, customer) {
            if (err) return res.status(500).json(createOutput(false, err));
            if (customer == null) return res.status(404).json(createOutput(false, {}));
            res.status(200).json(createOutput(true, {
                name:       customer.name,
                surname:    customer.surname,
                phone:      customer.phone
            }));
        });
    };

    self.removeUser = function(objectID, res){      // Removes user by id
        // callback is missing, we dont want to execute immediately
        var query = Customer.findOneAndRemove({_id: objectID});

        query.exec(function (err, customer) {
            if (err) return res.status(500).json(createOutput(false, err));
            if (customer == null) return res.status(404).json(createOutput(false, {}));
            res.status(200).json(createOutput(true, {}));
        });
    };

    self.updateUser = function(objectID, newParams, res){   // Updates user by id
        // callback is missing, we dont want to execute immediately
        var query = Customer.findById({_id: objectID});

        query.exec(function (err, customer) {
            if (err) return res.status(500).json(createOutput(false, err));
            if (customer == null) return res.status(400).json(createOutput(false, {})); // not found
            customer.name = newParams.name;
            customer.surname = newParams.surname;
            customer.phone = newParams.phone;
            customer.save();
            res.status(200).json(createOutput(true, {
                name:       customer.name,
                surname:    customer.surname,
                phone:      customer.phone
            }));
        });
    };
};

// Include our module at nodejs module exports
module.exports = mongoTools;
