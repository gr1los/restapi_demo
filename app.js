/**
 * Created by gabriel on 6/21/16.
 */

// Constants for http server's configuration.
const port = 3000;

var express = require("express");
var bodyParser = require("body-parser");
var mongoTools = require("./mongo_tools.js");


var router = express.Router();
var app = express();
var mongo = new mongoTools();

// Enable parser for url's body.
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Provide all routes here, this is for Home page.
router.get("",function(req, res){
    var d = new Date();
    res.end("Hello World!");
    console.log(
        "[Home] " + d.toDateString() + " " + d.toTimeString() + " " + req.hostname
    );
});

router.get("/:id", function (req, res) {    // Retrieve customer by id
    var d = new Date();
    mongo.findUser(req.params.id, res);
    console.log(
        "[Retrieve] " + d.toDateString() + " " + d.toTimeString() + " " + req.hostname
    );
});

router.post("/", function (req, res) {      // Create new customer
    var d = new Date();
    mongo.addUser(req.body, res);
    console.log(
        "[Create] " + d.toDateString() + " " + d.toTimeString() + " " + req.hostname
    );
});

router.put("/:id", function (req, res) {    // Update customer
    var d = new Date();
    mongo.updateUser(req.params.id, req.body, res);
    console.log(
        "[Update] " + d.toDateString() + " " + d.toTimeString() + " " + req.hostname
    );
});

router.delete("/:id", function (req, res) { // Delete customer
    var d = new Date();
    mongo.removeUser(req.params.id, res);
    console.log(
        "[Remove] " + d.toDateString() + " " + d.toTimeString() + " " + req.hostname
    );
});

// Tell express to use this router.
app.use("/",router);

app.listen(port, function () {
    console.log("Server running on port " + port + "!");
});
